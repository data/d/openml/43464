# OpenML dataset: UFC-Fights-(2010---2020)-with-Betting-Odds

https://www.openml.org/d/43464

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
There are some great UFC datasets out there, but I could not find one that included gambling odds. So I went and made one myself.  This dataset focuses very generally on the fights and hopes to be able to draw very broad conclusions.  More a more in depth statistical fight analysis I would recommend Rajeev Warrier's excellent datasetwhich was the inspiration for my work. 
Content
This dataset consists of 11 columns of data with basic information about every match that took place between March 21, 2010 and March 14, 2020.
Column Definitions:
R_fighter and B_fighter: The names of the fighter in the red corner and the fighter in the blue corner
R_odds and B_odds: The American odds of the fighter winning.  
date: The date of the fight
location: The location of the fight
country: The country the fight occurred in
Winner: The winner of the fight ('Red' or 'Blue')
title_bout: Was this fight a title bout? ('True' or 'False')
weight_class: What weight class did this fight occur at?
gender: Male or Female
Acknowledgements
I was inspired by the work of Rajeev Warrier
Want More?
My work, including a scraper to help gather data for upcoming events, can be found on my GitHub.  I promise I'll add more documentation soon.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43464) of an [OpenML dataset](https://www.openml.org/d/43464). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43464/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43464/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43464/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

